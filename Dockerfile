# Copyleft (c) December, 2021, Oromion.
# Usage: docker build -t dune-archiso/dune-archiso:dune-core .

FROM registry.gitlab.com/dune-archiso/images/dune-archiso

LABEL maintainer="Oromion <caznaranl@uni.pe>" \
  name="Dune extra-x86_64-build" \
  description="Dune in extra-x86_64-build" \
  url="https://gitlab.com/dune-archiso/images/dune-archiso-extra-x86_64-build/container_registry" \
  vcs-url="https://gitlab.com/dune-archiso/images/dune-archiso-extra-x86_64-build" \
  vendor="Oromion Aznarán" \
  version="1.0"

RUN sudo pacman -Syyuq --needed --noconfirm devtools && \
  pacman -Qtdq | xargs -r sudo pacman --noconfirm -Rcns && \
  sudo pacman -Scc <<< Y <<< Y && \
  sudo rm -r /var/lib/pacman/sync/*